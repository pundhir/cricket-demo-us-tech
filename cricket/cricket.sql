-- MySQL dump 10.13  Distrib 8.0.13, for macos10.14 (x86_64)
--
-- Host: localhost    Database: cricket_demo
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `matches`
--

DROP TABLE IF EXISTS `matches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `matches` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `match_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `match_place` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `match_date` datetime NOT NULL,
  `teama_id` bigint(20) unsigned DEFAULT NULL,
  `teamb_id` bigint(20) unsigned DEFAULT NULL,
  `winner_team_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `matches_teama_id_foreign` (`teama_id`),
  KEY `matches_teamb_id_foreign` (`teamb_id`),
  KEY `matches_winner_team_id_foreign` (`winner_team_id`),
  CONSTRAINT `matches_teama_id_foreign` FOREIGN KEY (`teama_id`) REFERENCES `team` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `matches_teamb_id_foreign` FOREIGN KEY (`teamb_id`) REFERENCES `team` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `matches_winner_team_id_foreign` FOREIGN KEY (`winner_team_id`) REFERENCES `team` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `matches`
--

LOCK TABLES `matches` WRITE;
/*!40000 ALTER TABLE `matches` DISABLE KEYS */;
INSERT INTO `matches` VALUES (1,'1st T20I','Delhi','2020-06-15 15:00:00',1,2,1,NULL,NULL),(2,'2nd T20I','Mumbai','2020-06-17 10:45:00',3,4,4,NULL,NULL),(3,'3rd T20I','Kolkata','2020-06-18 18:30:00',5,6,5,NULL,NULL),(4,'4th T20I','Chennai','2020-06-19 15:00:00',7,8,8,NULL,NULL),(5,'5th T20I','Delhi','2020-06-20 10:45:00',1,4,1,NULL,NULL),(6,'6th T20I','Kolkata','2020-06-21 18:30:00',5,8,5,NULL,NULL),(7,'7th T20I','Delhi','2020-06-02 15:00:00',1,5,1,NULL,NULL);
/*!40000 ALTER TABLE `matches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2020_05_03_105705_create_team_table',1),(4,'2020_05_03_110224_create_points_table',1),(5,'2020_05_03_110929_add_foreign_keys_to_points_table',1),(6,'2020_05_03_111302_create_player_table',1),(7,'2020_05_03_111705_add_foreign_keys_to_player_table',1),(8,'2020_05_03_111832_create_player_history_table',1),(9,'2020_05_03_112024_add_foreign_keys_to_player_history_table',1),(10,'2020_05_03_112318_create_matches_table',1),(11,'2020_05_03_112707_add_foreign_keys_to_matches_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `player`
--

DROP TABLE IF EXISTS `player`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `player` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `team_id` bigint(20) unsigned DEFAULT NULL,
  `first_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_uri` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `player_jersey_number` int(11) NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `player_team_id_foreign` (`team_id`),
  CONSTRAINT `player_team_id_foreign` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `player`
--

LOCK TABLES `player` WRITE;
/*!40000 ALTER TABLE `player` DISABLE KEYS */;
INSERT INTO `player` VALUES (1,1,'MS','Dhoni','ms_dhoni.png',7,'India',NULL,NULL),(2,1,'Suresh','Raina','suresh_raina.png',8,'India',NULL,NULL),(3,1,'Ravindra','Jadeja','ravindra_jadeja.png',9,'India',NULL,NULL),(4,1,'Ambati','Rayudu','ambati_rayudu.png',10,'India',NULL,NULL),(5,1,'Deepak','Chahar','deepak_chahar.png',11,'India',NULL,NULL),(6,1,'Dwayne','Bravo','dwayne_bravo.png',12,'West Indian',NULL,NULL),(7,1,'Harbhajan','Singh','harbhajan_singh.png',13,'India',NULL,NULL),(8,1,'Imran','Tahir','imran_tahir.png',14,'South African',NULL,NULL),(9,1,'Kedar','Jadhav','kedar_jadhav.png',15,'India',NULL,NULL),(10,1,'Km','Asif','km_asif.png',16,'India',NULL,NULL),(11,1,'Murali','Vijay','murali_vijay.png',17,'India',NULL,NULL),(12,2,'Amit','Mishra','Amit_Mishra_2.png',18,'India',NULL,NULL),(13,2,'Avesh','Khan','Avesh_Khan_2.png',19,'India',NULL,NULL),(14,2,'Axar','Patel','Axar_Patel_2.png',20,'India',NULL,NULL),(15,2,'Harshal','Patel','Harshal_Patel_2.png',21,'India',NULL,NULL),(16,2,'Ishant','Sharma','Ishant_Sharma_2.png',22,'India',NULL,NULL),(17,2,'Kagiso','Rabada','Kagiso_Rabada_2.png',23,'West Indian',NULL,NULL),(18,2,'Keemo','Paul','Keemo_Paul_2.png',24,'India',NULL,NULL),(19,2,'Prithvi','Shah','Prithvi_Shah_2.png',25,'India',NULL,NULL),(20,2,'Rishabh','Pant','Rishabh_Pant_2.png',26,'India',NULL,NULL),(21,2,'Shikhar','Dhawan','Shikhar_Dhawan_2.png',27,'India',NULL,NULL),(22,2,'Shreyas','Iyer','Shreyas_Iyer_2.png',28,'India',NULL,NULL),(23,3,'Arshdeep','Singh','Arshdeep_Singh_32.png',29,'India',NULL,NULL),(24,3,'Chris','Gayle','Chris_Gayle_33.png',30,'West Indian',NULL,NULL),(25,3,'Darshan','Nalkande','Darshan_Nalkande_34.png',31,'India',NULL,NULL),(26,3,'Harpreet','Brar','Harpreet_Brar_35.png',32,'India',NULL,NULL),(27,3,'Karun','Nair','Karun_Nair_36.png',33,'India',NULL,NULL),(28,3,'KL','Rahul','KL_Rahul_31.png',34,'India',NULL,NULL),(29,3,'Mandeep','Singh','Mandeep_Singh_37.png',35,'India',NULL,NULL),(30,3,'Mayank','Agarwal','Mayank_Agarwal_38.png',36,'India',NULL,NULL),(31,3,'Mohammad','Shami','Mohammad_Shami_39.png',37,'India',NULL,NULL),(32,3,'Murugan','Ashwin','Murugan_Ashwin_333.png',38,'India',NULL,NULL),(33,3,'Nicholas','Pooran','Nicholas_Pooran_30.png',39,'India',NULL,NULL),(34,4,'Andre','Russell','4_Andre_Russell.png',40,'India',NULL,NULL),(35,4,'Dinesh','Karthik','4_Dinesh_Karthik.png',41,'India',NULL,NULL),(36,4,'Harry','Gurney','4_HArry_Gurney.png',42,'India',NULL,NULL),(37,4,'Kamlesh','Nagarkpti','4_Kamlesh_Nagarkpti.png',43,'India',NULL,NULL),(38,4,'Kuldeep','Yadav','4_Kuldeep_Yadav.png',44,'India',NULL,NULL),(39,4,'Nitish','Rana','4_Nitish_Rana.png',45,'India',NULL,NULL),(40,4,'Pradish','Krishna','4_Pradish_Krishna.png',46,'India',NULL,NULL),(41,4,'Rinku','Singh','4_Rinku_Singh.png',47,'India',NULL,NULL),(42,4,'Sandeep','Warrier','4_Sandeep_warrier.png',48,'India',NULL,NULL),(43,4,'Shivam','Mavi','4_Shivam_Mavi.png',49,'India',NULL,NULL),(44,4,'Shubman','Gill','4_Shubman_Gill.png',50,'India',NULL,NULL),(45,5,'Aditya','Tare','5_Aditya_Tare.png',51,'India',NULL,NULL),(46,5,'Anmolpreet','Singh','5_Anmolpreet_Singh.png',52,'India',NULL,NULL),(47,5,'Anukul','Roy','5_Anukul_Roy.png',53,'India',NULL,NULL),(48,5,'Hardik','Pandya','5_Hardik_Pandya.png',54,'India',NULL,NULL),(49,5,'Ishan','kishan','5_Ishan_kishan.png',55,'India',NULL,NULL),(50,5,'Jaspreet','Bumraah','5_Jaspreet_Bumraah.png',56,'India',NULL,NULL),(51,5,'Jayant','Yadav','5_Jayant_Yadav.png',57,'India',NULL,NULL),(52,5,'Kieron','Pollard','5_Kieron_Pollard.png',58,'West Indian',NULL,NULL),(53,5,'Krunal','Pandya','5_Krunal_Pandya.png',59,'India',NULL,NULL),(54,5,'Lasith','Malinga','5_Lasith_Malinga.png',60,'India',NULL,NULL),(55,5,'Rohit','Sharma','5_Rohit_Sharma.png',61,'India',NULL,NULL),(56,6,'Ben','Stokes','6_Ben_Stokes.png',62,'India',NULL,NULL),(57,6,'Jofra','Archer','6_Jofra_Archer.png',63,'India',NULL,NULL),(58,6,'Jos','Butter','6_Jos_Butter.png',64,'India',NULL,NULL),(59,6,'Mahipal','Lomror','6_Mahipal_lomror.png',65,'India',NULL,NULL),(60,6,'Manan','Vohra','6_Manan_Vohra.png',66,'India',NULL,NULL),(61,6,'Riyan','Parag','6_Riyan_Parag.png',67,'India',NULL,NULL),(62,6,'Sanju','Samson','6_Sanju_Samson.png',68,'India',NULL,NULL),(63,6,'Shashank','Singh','6_Shashank_Singh.png',69,'India',NULL,NULL),(64,6,'Shreyas','Gopal','6_Shreyas_Gopal.png',70,'India',NULL,NULL),(65,6,'Steve','Smith','6_Steve_Smith.png',71,'India',NULL,NULL),(66,6,'Varun','Aaron','6_Varun_Aaron.png',72,'India',NULL,NULL),(67,7,'AB','Devilliers','7_AB_Devilliers.png',73,'India',NULL,NULL),(68,7,'Devdutt','Padikkal','7_Devdutt_Padikkal.png',74,'India',NULL,NULL),(69,7,'Gurkeerat Mann','Singh','7_Gurkeerat_Mann_Singh.png',75,'India',NULL,NULL),(70,7,'Moeen','Ali','7_Moeen_Ali.png',76,'India',NULL,NULL),(71,7,'Mohammed','Siraj','7_Mohammed_Siraj.png',77,'India',NULL,NULL),(72,7,'Navdeep','Saini','7_Navdeep_Saini.png',78,'India',NULL,NULL),(73,7,'Pawan','Negi','7_Parthiv_Patel.png',79,'India',NULL,NULL),(74,7,'asfas','asfas','7_Pawan_Negi.png',80,'India',NULL,NULL),(75,7,'Shivam','Dube','7_Shivam_Dube.png',81,'India',NULL,NULL),(76,7,'Umesh','Yadav','7_Umesh_Yadav.png',82,'India',NULL,NULL),(77,7,'Virat','Kohli','7_Virat_Kohli.png',83,'India',NULL,NULL),(78,8,'Abhishek','Sharma','8_Abhishek_Sharma.png',84,'India',NULL,NULL),(79,8,'Basil','Thampi','8_Basil_Thampi.png',85,'India',NULL,NULL),(80,8,'Bhuvneshwar','Kumar','8_Bhuvneshwar_Kumar.png',86,'India',NULL,NULL),(81,8,'Billy','Stanlake','8_Billy_Stanlake.png',87,'India',NULL,NULL),(82,8,'David','Warner','8_David_warner.png',88,'India',NULL,NULL),(83,8,'Jonny','Bairstow','8_Jonny_Bairstow.png',89,'West Indian',NULL,NULL),(84,8,'Kane','Williamson','8_Kane_Williamson.png',90,'India',NULL,NULL),(85,8,'Manish','Pandey','8_Manish_pandey.png',91,'India',NULL,NULL),(86,8,'Mohammad','Nabi','8_Mohammad_Nabi.png',92,'India',NULL,NULL),(87,8,'Rashid','Khan','8_Rashid_Khan.png',93,'India',NULL,NULL),(88,8,'Sansdeep','Sharma','8_Sansdeep_Sharma.png',94,'India',NULL,NULL);
/*!40000 ALTER TABLE `player` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `player_history`
--

DROP TABLE IF EXISTS `player_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `player_history` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `player_id` bigint(20) unsigned DEFAULT NULL,
  `matches` int(11) NOT NULL,
  `run` int(11) NOT NULL,
  `highest_scores` int(11) NOT NULL,
  `fifties` int(11) NOT NULL,
  `hundreds` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `player_history_player_id_foreign` (`player_id`),
  CONSTRAINT `player_history_player_id_foreign` FOREIGN KEY (`player_id`) REFERENCES `player` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `player_history`
--

LOCK TABLES `player_history` WRITE;
/*!40000 ALTER TABLE `player_history` DISABLE KEYS */;
INSERT INTO `player_history` VALUES (1,1,234,8675,183,55,36,NULL,NULL),(2,2,123,8752,103,45,31,NULL,NULL),(3,3,213,8675,153,35,16,NULL,NULL),(4,4,212,4677,123,25,26,NULL,NULL),(5,5,123,3635,181,15,32,NULL,NULL),(6,6,123,2645,113,45,33,NULL,NULL),(7,7,232,5625,185,21,37,NULL,NULL),(8,8,221,3655,153,22,31,NULL,NULL),(9,9,255,2695,186,12,22,NULL,NULL),(10,10,244,7665,173,5,11,NULL,NULL),(11,11,211,4635,143,25,8,NULL,NULL),(12,12,234,8675,183,55,36,NULL,NULL),(13,13,123,8752,103,45,31,NULL,NULL),(14,14,213,8675,153,35,16,NULL,NULL),(15,15,212,4677,123,25,26,NULL,NULL),(16,16,123,3635,181,15,32,NULL,NULL),(17,17,123,2645,113,45,33,NULL,NULL),(18,18,232,5625,185,21,37,NULL,NULL),(19,19,221,3655,153,22,31,NULL,NULL),(20,20,255,2695,186,12,22,NULL,NULL),(21,21,244,7665,173,5,11,NULL,NULL),(22,22,211,4635,143,25,8,NULL,NULL),(23,23,211,4635,143,25,8,NULL,NULL),(24,24,211,4635,143,25,8,NULL,NULL),(25,25,211,4635,143,25,8,NULL,NULL),(26,26,211,4635,143,25,8,NULL,NULL),(27,27,211,4635,143,25,8,NULL,NULL),(28,28,211,4635,143,25,8,NULL,NULL),(29,29,211,4635,143,25,8,NULL,NULL),(30,30,211,4635,143,25,8,NULL,NULL),(31,31,211,4635,143,25,8,NULL,NULL),(32,32,211,4635,143,25,8,NULL,NULL),(33,33,211,4635,143,25,8,NULL,NULL),(34,34,123,8752,103,45,31,NULL,NULL),(35,35,213,8675,153,35,16,NULL,NULL),(36,36,211,4635,143,25,8,NULL,NULL),(37,37,211,4635,143,25,8,NULL,NULL),(38,38,211,4635,143,25,8,NULL,NULL),(39,39,211,4635,143,25,8,NULL,NULL),(40,40,211,4635,143,25,8,NULL,NULL),(41,41,211,4635,143,25,8,NULL,NULL),(42,42,211,4635,143,25,8,NULL,NULL),(43,43,211,4635,143,25,8,NULL,NULL),(44,44,211,4635,143,25,8,NULL,NULL),(45,45,211,4635,143,25,8,NULL,NULL),(46,46,211,4635,143,25,8,NULL,NULL),(47,47,211,4635,143,25,8,NULL,NULL),(48,48,211,4635,143,25,8,NULL,NULL),(49,49,211,4635,143,25,8,NULL,NULL),(50,50,211,4635,143,25,8,NULL,NULL),(51,51,211,4635,143,25,8,NULL,NULL),(52,52,211,4635,143,25,8,NULL,NULL),(53,53,211,4635,143,25,8,NULL,NULL),(54,54,211,4635,143,25,8,NULL,NULL),(55,55,211,4635,143,25,8,NULL,NULL),(56,56,211,4635,143,25,8,NULL,NULL),(57,57,211,4635,143,25,8,NULL,NULL),(58,58,211,4635,143,25,8,NULL,NULL),(59,59,211,4635,143,25,8,NULL,NULL),(60,60,211,4635,143,25,8,NULL,NULL),(61,61,211,4635,143,25,8,NULL,NULL),(62,62,211,4635,143,25,8,NULL,NULL),(63,63,211,4635,143,25,8,NULL,NULL),(64,64,211,4635,143,25,8,NULL,NULL),(65,65,211,4635,143,25,8,NULL,NULL),(66,66,211,4635,143,25,8,NULL,NULL),(67,67,211,4635,143,25,8,NULL,NULL),(68,68,211,4635,143,25,8,NULL,NULL),(69,69,211,4635,143,25,8,NULL,NULL),(70,70,211,4635,143,25,8,NULL,NULL),(71,71,211,4635,143,25,8,NULL,NULL),(72,72,211,4635,143,25,8,NULL,NULL),(73,73,211,4635,143,25,8,NULL,NULL),(74,74,211,4635,143,25,8,NULL,NULL),(75,75,211,4635,143,25,8,NULL,NULL),(76,76,211,4635,143,25,8,NULL,NULL),(77,77,211,4635,143,25,8,NULL,NULL),(78,78,211,4635,143,25,8,NULL,NULL),(79,79,211,4635,143,25,8,NULL,NULL),(80,80,211,4635,143,25,8,NULL,NULL),(81,81,211,4635,143,25,8,NULL,NULL),(82,82,211,4635,143,25,8,NULL,NULL),(83,83,211,4635,143,25,8,NULL,NULL),(84,84,211,4635,143,25,8,NULL,NULL),(85,85,211,4635,143,25,8,NULL,NULL),(86,86,211,4635,143,25,8,NULL,NULL),(87,87,211,4635,143,25,8,NULL,NULL),(88,88,211,4635,143,25,8,NULL,NULL);
/*!40000 ALTER TABLE `player_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `points`
--

DROP TABLE IF EXISTS `points`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `points` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `team_id` bigint(20) unsigned DEFAULT NULL,
  `point` int(11) NOT NULL,
  `matches` int(11) NOT NULL,
  `won` int(11) NOT NULL,
  `lost` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `points_team_id_foreign` (`team_id`),
  CONSTRAINT `points_team_id_foreign` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `points`
--

LOCK TABLES `points` WRITE;
/*!40000 ALTER TABLE `points` DISABLE KEYS */;
INSERT INTO `points` VALUES (1,1,6,3,3,0,NULL,NULL),(2,2,0,1,0,1,NULL,NULL),(3,3,0,1,0,1,NULL,NULL),(4,4,2,2,1,1,NULL,NULL),(5,5,4,3,2,1,NULL,NULL),(6,6,0,1,0,1,NULL,NULL),(7,7,0,1,0,1,NULL,NULL),(8,8,2,2,1,1,NULL,NULL);
/*!40000 ALTER TABLE `points` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team`
--

DROP TABLE IF EXISTS `team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `team` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo_uri` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `club_state` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team`
--

LOCK TABLES `team` WRITE;
/*!40000 ALTER TABLE `team` DISABLE KEYS */;
INSERT INTO `team` VALUES (1,'Chennai Super Kings','chennai_super_king.png','Chennai',NULL,NULL),(2,'Delhi Capitals','delhi_capitals.png','Delhi',NULL,NULL),(3,'Kings Xi Punjab','kings_xi_punjab.jpeg','Punjab',NULL,NULL),(4,'Kolkata','kolkata_knight_riders.png','Kolkata',NULL,NULL),(5,'Mumbai Indians','mumbai_indians.png','Mumbai',NULL,NULL),(6,'Rajasthan Royals','rajasthan_royals.png','Rajasthan',NULL,NULL),(7,'Royal Challengers Bangalore','royal_challengers_bangalore.jpeg','Bangalore',NULL,NULL),(8,'Sunrisers Hyderabad','sunrisers_hyderabad.png','Hyderabad',NULL,NULL);
/*!40000 ALTER TABLE `team` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-03 21:00:40
