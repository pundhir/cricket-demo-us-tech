<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Model\Team;
use App\Http\Model\Match;
use App\Http\Model\Point;
use App\Http\Model\Player;
use App\Http\Model\PlayerHistory;
use Request;

class CricketController extends Controller
{
    /**
     * construct
     */
    public function __construct() {
        session_start();

        $this->objTeam = new Team;
        $this->objMatch = new Match;
        $this->objPoint = new Point;
        $this->objPlayer = new Player;
        $this->objPlayerHistory = new PlayerHistory;

    }

    /**
     * Show Team List
     * @param type $_GET
     * @return boolean
     */
    public function teamList(){

        $request = Request::input();

        $request['offset'] = 0;
        $request['limit'] = 1000;

        $DBResult = $this->objTeam->getTeams($request);
        $DBResultMatch = $this->objMatch->getMatches($request);
        $DBResultPoint = $this->objPoint->getPoints($request);

        $team_name = array();
        $team_image = array();

        if(!empty($DBResult)){
            foreach($DBResult as $res)
            {
                $team_name[$res->team_id] = $res->name;
                $team_image[$res->team_id] = $res->logo_uri;
            }
        }

        $data['Teams'] = $DBResult;
        $data['Matches'] = $DBResultMatch;
        $data['Team_Name'] = $team_name;
        $data['Team_Image'] = $team_image;
        $data['Points'] = $DBResultPoint;

        return view('team', compact('data'));
    }


    /**
     * get players List by team_id
     * @param type $_GET
     * @return boolean
     */
    public function players(){

        $request = Request::input();

        if(!empty($request['id'])){
            $id = base64_decode($request['id']);
            $name = base64_decode($request['name']);
        }

        $DBResultPlayers = $this->objPlayer->getPlayers(['id' => $id]);

        $data['Players'] = $DBResultPlayers;
        $data['Team_Name'] = $name;

        return view('player', compact('data'));

    }


}
