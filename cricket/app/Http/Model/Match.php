<?php

namespace App\http\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Match extends Model
{
    protected $table = 'matches';

    protected $fillable = ['match_id', 'match_name','match_place','match_date','teama_id','teamb_id','winner_team_id'];

    protected $dates = ['created_at', 'updated_at'];

    /**
     * get Matches List
     * @param type $value
     * @return boolean
     */
    public function getMatches($value = array()) {
        try {
            $DbQuery = DB::table("matches as M")
            ->select(DB::raw("M.id as match_id, M.match_name, M.match_place, M.match_date, M.teama_id as team_A, M.teamb_id as team_B, M.winner_team_id as winner_team"));

            if (!empty($value['offset'])) {
                $DbQuery->offset($value['offset']);
            }
            if (!empty($value['limit'])) {
                $DbQuery->limit($value['limit']);
            }
            $DbQuery->orderBy('M.id', 'ASC');
            return $DbQuery->get();
        } catch (Exception $exc) {
            return false;
        }
    }
}
