<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class PlayerHistory extends Model
{
    protected $table = 'player_history';

    protected $fillable = ['matches', 'run', 'highest_scores','fifties','hundreds'];

    protected $dates = ['created_at', 'updated_at'];

}
