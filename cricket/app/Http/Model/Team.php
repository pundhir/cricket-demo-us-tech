<?php

namespace App\http\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Team extends Model
{
    protected $table = 'team';

    protected $fillable = ['name', 'logo_uri','club_state'];

    protected $dates = ['created_at', 'updated_at'];

    /**
     * get Team List
     * @param type $value
     * @return boolean
     */

    public function getTeams($value = array()) {
        try {
            $DbQuery = DB::table("team as T")
            ->select(DB::raw("T.id as team_id, T.name, T.logo_uri"));

            if (!empty($value['offset'])) {
                $DbQuery->offset($value['offset']);
            }
            if (!empty($value['limit'])) {
                $DbQuery->limit($value['limit']);
            }
            $DbQuery->orderBy('T.id', 'ASC');
            return $DbQuery->get();
        } catch (Exception $exc) {
            return false;
        }
    }

}
