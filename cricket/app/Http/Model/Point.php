<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Point extends Model
{
    protected $table = 'points';

    protected $fillable = ['team_id', 'point', 'matches','won','lost'];

    protected $dates = ['created_at', 'updated_at'];


    /**
     * get Team Point List
     * @param type $value
     * @return boolean
     */
    public function getPoints($value = array()) {
        try {
            $DbQuery = DB::table("points as P")
            ->select(DB::raw("P.id as point_id, P.team_id, P.point, P.matches, P.won, P.lost, team.name as team_name, team.logo_uri as team_logo"));

            $DbQuery->leftJoin('team', function($join) {
                $join->on('P.team_id', '=', 'team.id');
            });

            if (!empty($value['offset'])) {
                $DbQuery->offset($value['offset']);
            }
            if (!empty($value['limit'])) {
                $DbQuery->limit($value['limit']);
            }
            $DbQuery->orderBy('P.id', 'ASC');
            return $DbQuery->get();
        } catch (Exception $exc) {
            return false;
        }
    }
}
