<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Player extends Model
{

    protected $table = 'player';

    protected $fillable = ['first_name', 'last_name', 'image_uri','player_jersey_number','country'];

    protected $dates = ['created_at', 'updated_at'];


    /**
     * get Team Players
     * @param type $value
     * @return boolean
     */
    public function getPlayers($value = array()) {
        try {
            $DbQuery = DB::table("player as P")
            ->select(DB::raw("P.id as player_id, P.first_name, P.last_name, P.image_uri, P.player_jersey_number, P.country, player_history.matches, player_history.run, player_history.highest_scores, player_history.fifties, player_history.hundreds"));

            $DbQuery->leftJoin('player_history', function($join) {
                $join->on('P.id', '=', 'player_history.player_id');
            });

            if (!empty($value['id'])) {
                $DbQuery->where("P.team_id", $value['id']);
            }
            //dd($DbQuery->toSql());
            return $DbQuery->get();
        } catch (Exception $exc) {
            return false;
        }
    }
}
