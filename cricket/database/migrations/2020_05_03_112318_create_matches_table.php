<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('match_name');
            $table->string('match_place');
            $table->dateTime('match_date');
            $table->bigInteger('teama_id')->nullable()->unsigned()->index('matches_teama_id_foreign');
            $table->bigInteger('teamb_id')->nullable()->unsigned()->index('matches_teamb_id_foreign');
            $table->bigInteger('winner_team_id')->nullable()->unsigned()->index('matches_winner_team_id_foreign');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matches');
    }
}
