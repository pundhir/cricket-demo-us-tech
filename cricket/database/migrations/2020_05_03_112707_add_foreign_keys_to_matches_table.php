<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('matches', function (Blueprint $table) {
            $table->foreign('teama_id')->references('id')->on('team')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('teamb_id')->references('id')->on('team')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('winner_team_id')->references('id')->on('team')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('matches', function (Blueprint $table) {
            $table->dropForeign('matches_teama_id_foreign');
            $table->dropForeign('matches_teamb_id_foreign');
            $table->dropForeign('matches_winner_team_id_foreign');
        });
    }
}
