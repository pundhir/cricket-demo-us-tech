<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayerHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('player_history', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('player_id')->nullable()->unsigned()->index('player_history_player_id_foreign');
            $table->integer('matches');
            $table->integer('run');
            $table->integer('highest_scores');
            $table->integer('fifties');
            $table->integer('hundreds');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('player_history');
    }
}
