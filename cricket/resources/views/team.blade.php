<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Teams</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <!-- Styles -->
        <style>
        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        table, th, td {
            border-collapse: collapse;
        }

        .table_team {
            margin: 15px;
            border: 2px solid red;
        }

        .table_match {
            margin: 15px;
            border: 2px solid orange;
        }

        .table_point {
            margin: 15px;
            border: 2px solid green;
        }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <table class="table_team" id="" border=1 >
                    <thead>
                    <tr>    <th colspan="2"><h3>Teams</h3></th>     </tr>

                    </thead>
                    <tbody>
                    @foreach ($data['Teams'] as $team)
                        <tr>
                            <td><a style="text-decoration:none" href="/players?id={{base64_encode($team->team_id)}}&name={{base64_encode($team->name)}}"><img src="/team_image/{{$team->logo_uri}}" style="width: 58px; height: 58px; border-radius: 2px;" alt=""> {{$team->name}}</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="content">
                <table class="table_match" id="" border=1 >
                    <thead>
                        <tr>    <th colspan="4"><h3>Matches Fixtures</h3></th>     </tr>
                    </thead>
                    <tbody>
                        @foreach ($data['Matches'] as $Match)
                            <tr>
                                <td align="center">{{$Match->match_name}}, {{date('D m M, H:i', strtotime($Match->match_date))}} (local) | {{$Match->match_place}} </br> <img src="/team_image/{{$data['Team_Image'][$Match->team_A]}}" style="width: 50px; height: 50px; border-radius: 2px;" alt=""> {{$data['Team_Name'][$Match->team_A]}} <b> V </b> <img src="/team_image/{{$data['Team_Image'][$Match->team_B]}}" style="width: 50px; height: 50px; border-radius: 2px;" alt=""> {{$data['Team_Name'][$Match->team_B]}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="content">
                <table class="table_point" id="" border=1 >
                    <thead>
                        <tr><th colspan="5"><h3>Points</h3></th></tr>
                        <tr>
                        <th>Team</th>
                        <th>Matches</th>
                        <th>Won</th>
                        <th>Lost</th>
                        <th>Points</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data['Points'] as $point)
                            <tr>
                                <td ><img src="/team_image/{{$point->team_logo}}" style="width: 58px; height: 58px; border-radius: 2px;" alt="">{{$point->team_name}}</td>
                                <td align="center">{{$point->matches}}</td>
                                <td align="center">{{$point->won}}</td>
                                <td align="center">{{$point->lost}}</td>
                                <td align="center">{{$point->point}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>
