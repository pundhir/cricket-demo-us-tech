<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Players</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <!-- Styles -->
        <style>
        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        table, th, td {
            border-collapse: collapse;
        }

        .table_team {
            margin: 15px;
            border: 2px solid red;
        }

        .table_match {
            margin: 15px;
            border: 2px solid orange;
        }

        .table_point {
            margin: 15px;
            border: 2px solid green;
        }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content flex-left">
                <div align="left"> <a style="text-decoration:none" href="/"> Back </a></div>
                <table class="table_team" id="" border=1 >
                    <thead>
                        <tr><th colspan="9"><h3>{{$data['Team_Name']}} Players</h3></th></tr>
                        <tr>
                        <th>Name</th>
                        <th>Image</th>
                        <th>Player Jersey Number</th>
                        <th>Country</th>
                        <th>Matches</th>
                        <th>Run</th>
                        <th>Highest Scores</th>
                        <th>Fifties</th>
                        <th>Hundreds</th>
                        </tr>
                    </thead>
                <tbody>
                @foreach ($data['Players'] as $player)
                    <tr>
                        <td align="center">{{$player->first_name}} {{$player->last_name}}</td>
                        <td align="center"><img src="/player_image/{{$player->image_uri}}" style="width: 58px; height: 58px; border-radius: 2px;" alt=""></td>
                        <td align="center">{{$player->player_jersey_number}}</td>
                        <td align="center">{{$player->country}}</td>
                        <td align="center">{{$player->matches}}</td>
                        <td align="center">{{$player->run}}</td>
                        <td align="center">{{$player->highest_scores}}</td>
                        <td align="center">{{$player->fifties}}</td>
                        <td align="center">{{$player->hundreds}}</td>
                    </tr>
                @endforeach
                </tbody>
                </table>
            </div>
        </div>
    </body>
</html>
